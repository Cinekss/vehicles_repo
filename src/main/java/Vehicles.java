import java.lang.reflect.Parameter;
import java.util.Map;

public class Vehicles {
    String producer;
    String vehType;

    Vehicles(String producer, String vehType){
        this.producer = producer;
        this.vehType = vehType;
    }

    @Override
    public String toString() {
        return "Vehicles{" +
                "producer='" + producer + '\'' +
                ", vehType='" + vehType + '\'' +
                '}';
    }

}

import java.util.*;
import java.util.logging.Logger;

public class Menu {
    public static Logger logger = Logger.getLogger(String.valueOf(Menu.class));

    public void menu() {
        //------------------------CAR------------------------------------
        Map<Vehicles, Integer> car = new HashMap<Vehicles, Integer>();
        car.put(new Vehicles("Ferrari","car"),250);
        car.put(new Vehicles("Porshe","car"),200);
        car.put(new Vehicles("BMW","car"),140);
        //------------------------BIKE------------------------------------
        Map<Vehicles, Integer> bike = new HashMap<Vehicles, Integer>();
        bike.put(new Vehicles("Scott","bike"),40);
        bike.put(new Vehicles("Giant","bike"),40);
        bike.put(new Vehicles("Trek","bike"),45);
        //------------------------PLANE------------------------------------
        Map<Vehicles, Integer> plane = new HashMap<Vehicles, Integer>();
        plane.put(new Vehicles("Boeing 767","plane"),840);
        plane.put(new Vehicles("Boeing 135","plane"),940);
        plane.put(new Vehicles("Boeing 363","plane"),845);
        //------------------------SHIP------------------------------------
        Map<Vehicles, Integer> ship = new HashMap<Vehicles, Integer>();
        ship.put(new Vehicles("Adratica","ship"),48);
        ship.put(new Vehicles("Tytanic","ship"),66);
        ship.put(new Vehicles("Pacyfica","ship"),56);



        Scanner scan = new Scanner(System.in);
        boolean exit = false;
        String option;
        Map.Entry<Vehicles, Integer> maxEntry = null;
        do {

            logger.info("\nYOUR POSSIBLE CHOICES:" + "\n1.car" + "\n2.bike" + "\n3.plane" + "\n4.ship" + "\n5.exit");

            logger.info("WHAT YOU WANT TO PICK? (car...exit): ");
            option = scan.nextLine();

            switch(option) {
                case "car":
                    for (Map.Entry<Vehicles, Integer> entry : car.entrySet())
                    {
                        if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0)
                        {
                            maxEntry = entry;
                        }
                    }
                    logger.info("Wszystkie samochody: " + String.valueOf(car));
                    logger.info("Najszybszy samochód: " + String.valueOf(maxEntry) + "km/h");
                    break;
                case "bike":
                    for (Map.Entry<Vehicles, Integer> entry : bike.entrySet())
                    {
                        if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0)
                        {
                            maxEntry = entry;
                        }
                    }
                    logger.info("Wszystkie rowery: " +String.valueOf(bike));
                    logger.info("Najszybszy rower: " +String.valueOf(maxEntry)+ "km/h");
                    break;
                case "plane":
                    for (Map.Entry<Vehicles, Integer> entry : plane.entrySet())
                    {
                        if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0)
                        {
                            maxEntry = entry;
                        }
                    }
                    logger.info("Wszystkie samoloty: " +String.valueOf(plane));
                    logger.info("Najszybszy samolot: " +String.valueOf(maxEntry)+ "km/h");
                    break;
                case  "ship":
                    for (Map.Entry<Vehicles, Integer> entry : ship.entrySet())
                    {
                        if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0)
                        {
                            maxEntry = entry;
                        }
                    }
                    logger.info("Wszystkie statki: " +String.valueOf(ship));
                    logger.info("Najszybszy statek: " +String.valueOf(maxEntry)+ "km/h");
                    break;

                case "exit":
                    System.exit(0);
                    break;

                default:
                    logger.info("Choose correct answer!");
            }

        }while (!exit);



    }

}
